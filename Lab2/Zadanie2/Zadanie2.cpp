﻿#include <iostream>
#include <string>
#include <Windows.h>
#include "assert.h"
using namespace std;


bool isRootFirstEquation(const int a, const int b, const int c, const int x) {
	return ((a*pow(x, 2) + b * x + c) == 0);
}

bool isRootSecondEquation(const int m, const int n, const int x) {
	return ((m*x + n) == 0);
}

int isRoot(const int a, const int b, const int c, const int x, const int m, const int n) {
	bool firstEquation = isRootFirstEquation(a, b, c, x);
	bool secondEquation = isRootSecondEquation(m, n, x);
	return firstEquation ^ secondEquation;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	assert(isRoot(1,2,3,4,5,6)==0);
	assert(isRoot(2, 3, -65, 5, 5, 6) == 1);
	assert(isRoot(2, 3, -65, 5, 2, -10) == 0);
	cout << "Введите данные уравнений." << endl;
	cout << "a: ";
	int a;
	cin >> a;
	cout << "b: ";
	int b;
	cin >> b;
	cout << "c: ";
	int c;
	cin >> c;
	cout << "x: ";
	int x;
	cin >> x;
	cout << "m: ";
	int m;
	cin >> m;
	cout << "n: ";
	int n;
	cin >> n;
	cout << isRoot(a, b, c, x, m, n);
}

