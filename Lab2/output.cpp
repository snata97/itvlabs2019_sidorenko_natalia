#include <string>
#include <iostream>
#include "output.h"
using namespace std;

void returnAsPrice(const float number, const string& str){
	int whole = static_cast<int>(abs(number));
	int fractional = static_cast<int>((abs(number) - whole)*100);
	cout << str << whole <<" ���. " << fractional << " ���.";
}

float EnterData(const string& varName) {
	cout << varName;
	float data;
	cin >> data;
	return data;
}

int getWhole(const float number) {
	return static_cast<int>(abs(number));
}

float getFractional(const float number) {
	return (number - getWhole(number));
}

float conversionToCashFormat(const float number) {
	int whole = getWhole(number);
	float fractional = getFractional(number);
	int firstDigitFractional = static_cast<int>(getWhole(fractional * 10));
	int secondDigitFractional = static_cast<int>(getFractional((fractional + 0.0005f)* 10)* 10);
	int thirdDigitFractional = static_cast<int>(getFractional((fractional + 0.0005f) * 100)) * 10;
	if (thirdDigitFractional > 5) {
		secondDigitFractional++;
	}
	return (whole + (static_cast<float>(firstDigitFractional * 10 + secondDigitFractional)) / 100);
};

bool isEqual(const float res, const float cost) {
	const double eps = 0.1;
	return abs(res - cost) < eps ? 1 : 0;
}