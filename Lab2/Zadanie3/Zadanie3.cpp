﻿#include <iostream>
#include <string>
#include <Windows.h>
#include "assert.h"
#include "output.h"

using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	assert(isEqual(12.35f, conversionToCashFormat(12.348f)) == 1);
	assert(isEqual(12.13f, conversionToCashFormat(12.128f)) == 1);
	assert(isEqual(12.12f, conversionToCashFormat(12.123f)) == 1);
	assert(isEqual(12.15f, conversionToCashFormat(12.15f)) == 1);
	assert(isEqual(12.50f, conversionToCashFormat(12.5f)) == 1);
	cout << "Преобразование числа в денежный формат." << endl;
	const float number = EnterData("Введите дробное число – ");
	const float res = conversionToCashFormat(number);
	char s[128] = "";
	sprintf_s(s, sizeof(s), "%f руб. - это", res);
	returnAsPrice(res, string(s));
	return 0;
}
