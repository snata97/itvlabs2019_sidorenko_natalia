﻿#include <iostream>
#include <string>
#include <Windows.h>
#include "assert.h"
#include "output.h"

using namespace std;

float costCalculation(const float distance, const float gasConsumption, const float gasPrice) {
	return (((distance*(gasConsumption / 100)*gasPrice)*2) / 100) * 100;
};

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	const double eps = 0.01;
	assert(isEqual(269.94f,costCalculation(67.f, 8.5f, 23.7f)) == 1);
	assert(isEqual(0.f,costCalculation(67.f, 0.f, 23.7f)) == 1);
	assert(isEqual(0.f,costCalculation(0.f,0.f,0.f)) == 1);
	cout << "Вычисление стоимости поездки на дачу."<<endl;
	const float distance = EnterData("Расстояние до дачи(км) – ");
	const float gasConsumption = EnterData("Расход бензина (л на 100 км) – ");
	const float gasPrice = EnterData("Цена литра бензина (руб.) – ");
	const string str = "Поездка на дачу обойдется в ";
	const float cost = costCalculation(distance, gasConsumption, gasPrice);
	const float res = conversionToCashFormat(cost);
	returnAsPrice(res, str);
	return 0;
}
