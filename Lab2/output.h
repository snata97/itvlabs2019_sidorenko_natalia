#pragma once

#include <string>

void returnAsPrice(const float,const std::string&);

float EnterData(const std::string&);

int getWhole(const float);

float getFractional(const float);

float conversionToCashFormat(const float);

bool isEqual(const float, const float);