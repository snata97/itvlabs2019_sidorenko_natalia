﻿//•	Написать приложение, которое находит сумму первых трех цифр 
//дробной части вещественного числа. Например, для числа 23,16809 она равна 15.

#include <iostream>
#include "assert.h"
using namespace std;

int sum(double number) {
	int whole = floor(abs(number));
	double fractional = abs(number) - whole;
	int threeDigits = floor(fractional * 1000);
	int result = 0;
	while (threeDigits != 0) {
		result += threeDigits % 10;
		threeDigits /= 10;
	}
	return result;
}

int main()
{
	assert(sum(23.16809) == 15);
	assert(sum(0.15) == 6);
	assert(sum(1.000) == 0);
	assert(sum(2.100) == 1);
	assert(sum(-2.100) == 1);
	assert(sum(3.5678) == 18);
	cout << "Enter a real number: ";
	float number;
	cin >> number;
	int result = sum(number);
	cout << "Result: " << result;
	cout << endl;
}

